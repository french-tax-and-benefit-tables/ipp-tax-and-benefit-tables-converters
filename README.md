# ipp-tax-and-benefit-tables-converters

Scripts to convert IPP's tax and benefit tables from XLS & XLSX to other formats (YAML, ...)

IPP = [Institut des politiques publiques](http://www.ipp.eu/en/)

Original tax and benefit tables, in XLSX format:
- English: http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
- French: http://www.ipp.eu/fr/outils/baremes-ipp/

## Data transformation pipelines

### In the IPP world

[XLS](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx)
[→](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters/blob/master/ipp_tax_and_benefit_tables_xls_to_yaml.py)
[raw YAML](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-raw)
[→](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters/blob/master/clean_up_ipp_tax_and_benefit_tables_yaml.py)
[clean YAML](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-clean)
[→](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters/blob/master/ipp_tax_and_benefit_tables_yaml_to_taxipp_csv.py)
[CSV for taxipp](https://framagit.org/french-tax-and-benefit-tables/taxipp-parameters)

Transformation scripts are represented by arrows, which are links to the scripts.

Those scripts are triggered by GitLab-CI. For example, the first repository with XLS files defines its own [`.gitlab-ci.yml`](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/blob/master/.gitlab-ci.yml) file.

About the steps:
- XLS files are kept in a git repository to keep history.
- Raw YAML files are a direct representation of Excel spreadsheets. They are stored in a git repository to ease navigating in history, since XLS files are binary. File [`WARNINGS.yaml`](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-raw/blob/master/WARNINGS.yaml) is written during the XLS to raw YAML transformation.
- Clean YAML files are validated versions of raw YAML files, with some default values inferred. Files [`ERRORS.yaml`](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-clean/blob/master/ERRORS.yaml) and [`WARNINGS.yaml`](https://framagit.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-clean/blob/master/WARNINGS.yaml) are written during the raw YAML to clean YAML transformation.
- CSV files are consumed by [taxipp](http://www.ipp.eu/outils/taxipp-outils/), the simulator of the IPP still in use today. They are not useful for OpenFisca.

### In the OpenFisca world

See https://github.com/openfisca/openfisca-france/tree/master/openfisca_france/scripts/parameters/baremes_ipp

## Copyright and License

```
These scripts are part of OpenFisca.

OpenFisca -- A versatile microsimulation software
By: OpenFisca Team <contact@openfisca.fr>

Copyright (C) 2011, 2012, 2013, 2014, 2015 OpenFisca Team
https://www.openfisca.fr/

OpenFisca is free software; you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

OpenFisca is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
