#! /usr/bin/env python
# -*- coding: utf-8 -*-


# OpenFisca -- A versatile microsimulation software
# By: OpenFisca Team <contact@openfisca.fr>
#
# Copyright (C) 2011, 2012, 2013, 2014, 2015 OpenFisca Team
# http://www.openfisca.fr/
#
# This file is part of OpenFisca.
#
# OpenFisca is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OpenFisca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Convert IPP's tax and benefit tables from XLSX format to XLS.

IPP = Institut des politiques publiques

Source XLSX files :
    http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
    http://www.ipp.eu/fr/outils/baremes-ipp/
"""


import argparse
import logging
import os
import subprocess
import sys


app_name = os.path.splitext(os.path.basename(__file__))[0]
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--source-dir', default = 'xlsx',
        help = 'path of source directory containing IPP XLSX files')
    parser.add_argument('-t', '--target-dir', default = 'xls',
        help = 'path of target directory for generated XLS files')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    # file_system_encoding = sys.getfilesystemencoding()

    if not os.path.exists(args.target_dir):
        os.makedirs(args.target_dir)
    obsolete_target_files_path_encoded = set(
        os.path.join(args.target_dir, filename_encoded)
        for filename_encoded in os.listdir(args.target_dir)
        if filename_encoded.endswith('.xls')
        )

    for filename_encoded in os.listdir(args.source_dir):
        if not filename_encoded.endswith('.xlsx'):
            continue
        source_file_path_encoded = os.path.join(args.source_dir, filename_encoded)
        target_file_path_encoded = os.path.join(args.target_dir, '{}.xls'.format(os.path.splitext(filename_encoded)[0]))
        subprocess.check_call(['ssconvert', '--export-type=Gnumeric_Excel:excel_biff8', source_file_path_encoded,
            target_file_path_encoded])
        obsolete_target_files_path_encoded.discard(target_file_path_encoded)

    # Delete obsolete files.
    for obsolete_target_file_path_encoded in obsolete_target_files_path_encoded:
        os.remove(obsolete_target_file_path_encoded)

    return 0


if __name__ == "__main__":
    sys.exit(main())
